var histIndex = 0;

$(document).ready(function () {
    /* On detecte l'OS */
    detectOs();
    /* On recupère le current path */
    getCurrentDirectory();
    /* On selectionne le terminal */
    selectTerminal();
    /* On gere le local storage pour l'historique des commandes */
    initHistoric();


    /**
     * Bouton de changement de thème de couleur
     */
    $("#switch-theme").click(function () {
        console.log(("it work"));
        switchTheme();
    });
});