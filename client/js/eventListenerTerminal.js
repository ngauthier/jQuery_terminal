/**
 * Gestion de la touche entrée
 */
$(window).keypress(function (e) {

    if (e.which == 13) {
        saveCommandEntered($("#input-term").val());
        commandLineEnter();
        histIndex = 0;
        clearInputTerminal();
        autoScrollBottom();
    }
});

/**
 * Gestion des arrowDOwn et arrowUp pour historique des commandes
 */
$(window).keydown(function (e) {
    if (e.which == 38) {
        growHistIndex();
        getItemInHistory();
    }

    if (e.which == 40) {
        reduceHistIndex();
        getItemInHistory();
    }
});
