package fr.imie.lightapi.restservice.controller;

import fr.imie.lightapi.restservice.model.Directory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Controller de la classe Directory, permettant d'effectuer toutes
 * les operations en lien avec les dossier et fichiers, et également
 * les quelques autres commandes demandé pour le TP
 */
@RestController
@RequestMapping("api/directory")
public class DirectoryController {

    private Directory directory = new Directory('\\');

    @PostMapping(path = "os")
    public ResponseEntity getSeparatorByOs(@RequestParam(value = "separator")String separator) {
        this.directory.setSeparator(separator.charAt(0));
        return new ResponseEntity(HttpStatus.OK);
    }

    /** cmd date */
    @GetMapping(path = "date", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDate() {
        return new ResponseEntity<>(this.directory.getFormatedDate(), HttpStatus.OK);
    }

    /** cmd java */
    @GetMapping(path = "version", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getJavaVersion() {
        return new ResponseEntity<String>("La version java utilisé est la version suivante :\nJava" + this.directory.getJavaVersion(), HttpStatus.OK);
    }

    /** cmd pwd */
    @GetMapping(path = "current", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCurrentPath() {
        return new ResponseEntity<>(this.directory.getCurrentDirectory(), HttpStatus.OK);
    }

    /** cmd get home */
    @GetMapping(path = "home", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getRootPath() {
        return new ResponseEntity<>(this.directory.getRootDirectory(), HttpStatus.OK);
    }

    /** cmd cd .. */
    @PostMapping(path = "cd/under", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changeToUnderDirectory() {
        try {
            return new ResponseEntity<>(this.directory.changePreviousDirectory(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /** cmd cd directory */
    @PostMapping(path = "cd/{directory}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> changeDirectory(@PathVariable(value = "directory") String directory) {
        String newCurrent;
        try {
            newCurrent = this.directory.changeDirectory(directory);
            return new ResponseEntity<>(newCurrent, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /** cmd mkdir dirname */
    @PostMapping(path = "mkdir/{dirname}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createDirectory(@PathVariable(value = "dirname") String dirname) {
        return this.directory.createDirectory(dirname) ? new ResponseEntity(HttpStatus.CREATED) : new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /** cmd rmdir dirname */
    @DeleteMapping(path = "rmdir/{dirname}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> removeDirectory(@PathVariable(value = "dirname") String dirname) {
        boolean status;
        try {
            status = this.directory.deleteDirectory(dirname);
            return new ResponseEntity<>(status ? HttpStatus.OK : HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /** cmd ls */
    @PostMapping(path = "ls", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> listSegmentsCurrent() {
        try {
            return new ResponseEntity<>(this.directory.listSegments(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /** cmd cat filename */
    @PostMapping(path = "cat", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> catFile(@RequestParam(name = "filename") String filename) {
        try {
            return new ResponseEntity<>(this.directory.catFile(filename), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    /** cmd ~ */
    @GetMapping(path = "gohome", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> goHome() {
        return new ResponseEntity<>(this.directory.goHome(), HttpStatus.OK);
    }

    /** cmd free */
    @GetMapping(path = "free", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Long>> getFree() {
        return new ResponseEntity<>(this.directory.getAvailableSpace(), HttpStatus.OK);
    }
}
