##JQuery Terminal

Serveur : SpringBootAPI (v2.0.1)

Client : Stack Web Classique (HTML CSS JS )

## How to run server ( with maven )

$> mvn dependency:resolve

$> mvn clean install

#En buildant un jar

$> mvn clean package

$> cd target

$> java -jar {jarname}

#En liveserver

$> mvn spring-boot:run